import Vue from 'vue'
import Router from 'vue-router'
import Rent from '@/components/Rent'
import Reservation from '@/components/Reservation'
import UserProfile from '@/components/UserProfile'
import UserHome from '@/components/UserHome'
Vue.use(Router)
export default new Router({
  mode: 'history',
  routes: [

    {
      path: '/',
      name: 'Rent',
      component: Rent
    },
    {
      path: '/Reservations/:reservation_id',
      name: 'Reservation',
      component: Reservation,
      children: [
        {
          name: 'UserProfile',
          path: 'UserProfile',
          component: UserProfile
        }
      ]
    },
    {
      path: '/Reservations/',
      component: Reservation,
      children: [
        {
          name: 'UserHome',
          path: '',
          component: UserHome
        }
      ]
    }
  ]
})
